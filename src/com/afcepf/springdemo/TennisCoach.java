package com.afcepf.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {
	// field injection
	@Autowired
	@Qualifier("randomFortuneService")
	public FortuneService fortuneService;
	
	public TennisCoach() {
		System.out.println(">> TennisCoach : inside default constructor");
	}
	
	// define a setter metho	
//	@Autowired
//	public void setFortuneService(FortuneService fortuneService) {
//		System.out.println(">> TennisCoach : inside default setFortuneService() method");
//		this.fortuneService = fortuneService;
//	}

	// Constructor injection
//    @Autowired
//	public TennisCoach(FortuneService fortuneService) {
//		this.fortuneService = fortuneService;
//	}

	 
	@Override
	public String getDailyWorkout() {
		
		
		return "Pratice your backhand volley";
	}


	@Override
	public String getDailyFortune() {
	
		return fortuneService.getFortune();
	}

}
